public class TEST{
    private VIERGEWINNT spiel;
    private boolean run = true;

    public TEST(){
        spieleTestSpiel();
    }

    public void spieleTestSpiel(){
        SPIELER alex = new SPIELER("Alex", 'A');
        spiel = new VIERGEWINNT(5,7,4,alex);

        spiel.spielerHinzufuegen("Yannick", 'Y');
        spiel.spielerHinzufuegen("Padde", 'P');
        spiel.spielerHinzufuegen("Felix", 'F');
        
        while(run){
            int i = 1 + (int)(Math.random() * ((5 - 1) + 1));
            System.out.println("Setze Stein auf " + i);
            if(spiel.steinSetzen(i))
                return;
        }
    }
    
    public void steinSetzen(int x){
        spiel.steinSetzen(x);   
    }
}