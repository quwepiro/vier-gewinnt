public class SPIELER{
    private String name;
    private char symbol;

    SPIELER(String name, char symbol){
        this.name = name;
        this.symbol = symbol;
    }

    public String gibName(){
        return name;
    }

    public char gibSymbol(){
        return symbol;
    }
}