public class VIERGEWINNT{
    private SPIELFELD spielfeld;
    private SPIELER[] spieler;
    private int aktuellerSpieler;

    VIERGEWINNT(int laenge, int breite, int siegReihe, SPIELER... spieler){
        this.spieler = spieler;
        neuesFeld(laenge, breite, siegReihe);
    }

    public void neuesFeld(int laenge, int breite, int siegReihe){ 
        if(laenge < 1)
            laenge = 1;
        if(breite < 1)
            breite = 1;
        if(siegReihe < 1)
            siegReihe = 1;
        if(breite < siegReihe)
            breite = siegReihe;

        spielfeld = new SPIELFELD(laenge, breite, siegReihe);
        System.out.print("Neues Spielfeld mit " + laenge + "x" + breite + " und den Spielern ");
        for(int i = 0;i<spieler.length;i++){
            System.out.print(spieler[i].gibName() + " (" + spieler[i].gibSymbol() + "), ");
        }
        System.out.println();
        System.out.println("Spieler " + spieler[aktuellerSpieler].gibName() + " (" + spieler[aktuellerSpieler].gibSymbol() + ") ist am Zug");
    } 

    public void spielerHinzufuegen(SPIELER neuerSpieler){
        SPIELER[] neueSpieler = new SPIELER[spieler.length+1];

        for(int i = 0;i<spieler.length;i++){
            neueSpieler[i] = spieler[i];
        }
        neueSpieler[spieler.length] = neuerSpieler;

        spieler = neueSpieler;

        System.out.println("Spieler " + neuerSpieler.gibName() + " (" + neuerSpieler.gibSymbol() + ") hinzugefügt");
    }

    public void spielerHinzufuegen(String name, char symbol){
        spielerHinzufuegen(new SPIELER(name,symbol));
    }

    public void spielerEntfernen(int index){
        if(index < 0 || index >= spieler.length || spieler.length == 1)
            return;

        SPIELER alterSpieler = spieler[index];
        SPIELER[] neueSpieler = new SPIELER[spieler.length-1];

        int j = 0;
        for(int i = 0;i<spieler.length;i++){
            if(i!=index){
                neueSpieler[j] = spieler[i];
                j++;
            }
        }

        spieler = neueSpieler;

        System.out.println("Spieler " + alterSpieler.gibName() + " (" + alterSpieler.gibSymbol() + ") entfernt");

        if(aktuellerSpieler >= neueSpieler.length){
            spielerWechseln();
        }
    }

    public void spielerEntfernen(SPIELER spieler){
        for(int i = 0;i<this.spieler.length;i++){
            if(this.spieler[i].gibName().equals(spieler.gibName()) && this.spieler[i].gibSymbol() == spieler.gibSymbol())
                spielerEntfernen(i);
        }
    }

    public void spielerEntfernen(String name, char symbol){
        spielerEntfernen(new SPIELER(name, symbol));
    }

    public boolean steinSetzen(int reihe){ 
        reihe--;

        if(reihe < 0 || reihe >= spielfeld.gibBreite()){
            System.out.println("Ungültige Reihe: " + (reihe+1));
            return false;
        }

        if(spielfeld.istVoll()){
            System.out.println("Das Spielfeld ist voll");
            spielfeld.reset();
            return true;
        }

        int y = spielfeld.steinSetzen(reihe, spieler[aktuellerSpieler]);

        if(y < 0){
            System.out.println("Reihe schon voll!");
            return false;
        }

        ausgeben();

        boolean win = spielfeld.siegPruefen(reihe,y);
        if(win){
            System.out.println("Spieler " + spieler[aktuellerSpieler].gibName() + " (" + spieler[aktuellerSpieler].gibSymbol() + ") hat gewonnen!!!");
            spielfeld.reset();
        }

        if(spielfeld.istVoll()){
            System.out.println("Das Spielfeld ist voll");
            spielfeld.reset();
            return true;
        }

        if(!win)
            spielerWechseln();
        return win;
    }

    private void ausgeben(){ 
        for(int y = spielfeld.gibHoehe()-1; y >= 0; y--){
            for(int x = 0; x < spielfeld.gibBreite();x++){
                SPIELER feld = spielfeld.gibSpielerVon(x,y);
                if(feld==null){ 
                    System.out.print("[]\t"); 
                } else {  
                    System.out.print(feld.gibSymbol()+"\t"); 
                }
            }
            System.out.println();
        }
    }

    private void spielerWechseln(){
        if(aktuellerSpieler >= spieler.length-1){
            aktuellerSpieler = 0;
        }
        else{
            aktuellerSpieler++;
        }

        System.out.println("Spieler " + spieler[aktuellerSpieler].gibName() + " (" + spieler[aktuellerSpieler].gibSymbol() + ") ist am Zug");
    }
}