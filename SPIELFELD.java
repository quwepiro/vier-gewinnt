public class SPIELFELD{
    private SPIELER[][] spielfeld;
    private int breite,hoehe, siegReihe;

    SPIELFELD(int breite, int hoehe, int siegReihe){
        this.breite = breite;
        this.hoehe = hoehe;
        this.siegReihe = siegReihe;
        reset();
    }

    public int gibBreite(){
        return breite;
    }

    public int gibHoehe(){
        return hoehe;
    }

    public void reset(){
        spielfeld = new SPIELER[breite][hoehe];
    } 

    public int steinSetzen(int reihe, SPIELER spieler){
        for (int i = 0; i < spielfeld[reihe].length; i++){
            if( spielfeld [reihe][i] == null){
                spielfeld[reihe][i] = spieler;
                return i;
            }
        }
        return -1;
    }

    public boolean siegPruefen(int x, int y){ 
        SPIELER sp = spielfeld[x][y]; 
        int xMax = spielfeld.length; 
        int yMax = spielfeld[0].length; 
        int wc = siegReihe -1;
        int check = 0;

        for(int i = 1; i<=wc; i++){
            if(y-i >= 0){
                if(sp.equals(spielfeld[x][y-i])){
                    check++;
                }
                else{
                    check = 0;
                }
                if(check == siegReihe){
                    return true;
                }
            }
        }
        check = 0;

        for(int i = -wc; i<=wc; i++){
            if(x+i >= 0 && x+i < xMax && y+i >= 0 && y+i < yMax){
                if(sp.equals(spielfeld[x+i][y+i])){
                    check++;
                }
                else{
                    check = 0;
                }
                if(check == siegReihe){
                    return true;
                }
            }
        }
        check = 0;

        for(int i = -wc; i<=wc; i++){
            if(x+i >= 0 && x+i < xMax){
                if(sp.equals(spielfeld[x+i][y])){
                    check++;
                }
                else{
                    check = 0;
                }
                if(check == siegReihe){
                    return true;
                }
            }
        }
        check = 0;

        for(int i = -wc; i<=wc; i++){
            if(x+i > xMax && y-i > yMax && x+i >= 0 && y-i >= 0){
                if(sp.equals(spielfeld[x+i][y-i])){
                    check++;
                }
                else{
                    check = 0;
                }
                if(check == siegReihe){
                    return true;
                }
            }
        }

        return false;
    }

    public boolean istVoll(){
        boolean voll = true;
        for(int i = 0;i<spielfeld.length;i++){
            for(int y = 0;y<spielfeld[i].length;y++){
                if(spielfeld[i][y] == null)
                    voll = false;
            }
        }
        return voll;
    }

    public SPIELER gibSpielerVon(int x, int y){ 
        return spielfeld[x][y]; 
    } 
}
